# Vue DevUI

Vue3版本的DevUI组件库，基于[https://github.com/devcloudfe/ng-devui](https://github.com/devcloudfe/ng-devui)

DevUI官方网站：[https://devui.design](https://devui.design)

Tips: 该项目目前还处于孵化和演进阶段，欢迎大家一起参与建设🤝

目前，组件移植的基本流程已经打通，欢迎大家参与到 Vue DevUI 项目的建设中来！👏🎉

通过参与 Vue DevUI 项目，你可以：
- 学习最新的 Vite+Vue3+TSX 技术
- 学习如何设计和开发组件
- 参与到开源社区中来
- 结识一群热爱学习、热爱开源的朋友

以下是项目进展和规划：
[项目进展和规划](https://gitee.com/devui/vue-devui/wikis/%E9%A1%B9%E7%9B%AE%E8%BF%9B%E5%B1%95%E5%92%8C%E8%A7%84%E5%88%92)

# 快速开始

## 1 安装依赖

```
yarn(推荐)

or

npm i
```

## 2 启动

```
yarn dev(推荐)

or

npx vite

or

npm run dev
```

## 3 访问

[http://localhost:3000/](http://localhost:3000/)

## 4 生产打包

```
yarn build(推荐)

or

npm run build
```

# 贡献者花名册

排名不分先后（按首字母排序）

## brenner8023

[brenner8023](https://gitee.com/brenner8023)

目前主要负责：
- Jest 单元测试环境搭建
- Radio 单选框
- CheckBox 复选框
- Switch 开关
- TagsInput 标签输入
- TextInput 文本框

## flxy1028(fang-ng4)

[flxy1028](https://github.com/flxy1028)

目前负责的责任田有：
- Tabs 选项卡

## kagol

[kagol](https://github.com/kagol)

目前负责的责任田有：
- Accordion 手风琴

## lwl(laiweilun)

[lwl](https://gitee.com/laiweilun)

- Cascader 级联菜单
- Search 搜索

## RootWater(lel)

[RootWater](https://gitee.com/RootWater)

- Toast 全局通知

## sise209(duqingyu)

[sise209](https://gitee.com/sise209)

- Badge 徽标
- ImagePreview 图片预览

## to0simple(NAN)

[to0simple](https://github.com/to0simple)

目前负责的责任田有：
- Alert 警告
- Avatar 头像
- Panel 面板
- Codebox 代码盒子
- Highlight 代码高亮

## vergil_lu(Vergil)

[vergil_lu](https://gitee.com/vergil_lu)

- Card 卡片

## xiao_jius_father(小九九的爸爸)

[xiao_jius_father](https://gitee.com/xiao_jius_father)

- Slider 滑动输入条
- Tooltip 提示

## Zcating

[Zcating](https://github.com/Zcating)

目前负责的责任田有：
- Button 按钮

## zzzautumn(~zZ.Lucky)

[zzzautumn](https://gitee.com/zzzautumn)

目前负责的责任田有：
- Rate 等级评估
